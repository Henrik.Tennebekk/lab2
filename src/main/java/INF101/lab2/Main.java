package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;


public class Main {
    public static IPokemon pokemon1;
    public static IPokemon pokemon2;

    public static void main(String[] args) {
        Main.pokemon1 = new Pokemon("pikachu");
        Main.pokemon2 = new Pokemon("ratata");
        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println();
  
        //Her angriper pokemonene hverandre til en a dem er død
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            //Angriper med pokemon 1
            if (pokemon1.isAlive()) {
                pokemon1.attack(pokemon2);
            }
            //Angriper med pokemon 2
            if (pokemon2.isAlive()) {
                pokemon2.attack(pokemon1);
            }          
        }
    }
}
